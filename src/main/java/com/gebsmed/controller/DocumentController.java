package com.gebsmed.controller;

import java.util.ArrayList;
import java.util.List;

import com.gebsmed.dao.DocumentDao;
import com.gebsmed.dao.DocumentStageDao;
import com.gebsmed.dao.ProductDao;
import com.gebsmed.dao.UserDao;
import com.gebsmed.model.Document;
import com.gebsmed.model.DocumentStage;
import com.gebsmed.model.GebsmedUser;
import com.gebsmed.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DocumentController {

	@Autowired
	DocumentDao docDao;

	@Autowired
	ProductDao productDao;

	@Autowired
	UserDao userDao;

	@Autowired
	DocumentStageDao docStageDao;

	// CREATE
	@RequestMapping(value = "/document/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity createCompany(
			@RequestBody Document document) {

		try {

			Product product = productDao.findOne(document.getProduct().getId());
			GebsmedUser user = userDao.findOne(document.getUser().getIdUser());
			DocumentStage stage = docStageDao.findOne(document
					.getDocumentStage().getStageId());

			document.setProduct(product);
			document.setUser(user);
			document.setDocumentStage(stage);

			docDao.save(document);

		} catch (Exception e) {

			return new ResponseEntity<>(document, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(document, HttpStatus.OK);
	}

	// UPDATE
	@RequestMapping(value = "/document/update/", method = RequestMethod.PATCH)
	public @ResponseBody ResponseEntity updateDocument(
			@RequestBody Document document) {

		Document edited = null;
		try {
			edited = docDao.findOne(document.getDocumentId());
			edited.setName(document.getName());
			edited.setDocumentStage(docStageDao.findOne(document
					.getDocumentStage().getStageId()));
			edited.setObjectKey(document.getObjectKey());
			edited.setDate(document.getDate());
			docDao.save(edited);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(edited, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/document/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteDocument(@PathVariable int id) {

		Document deleted = null;
		try {
			deleted = docDao.findOne(id);
			docDao.delete(id);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	// GET ONE
	@RequestMapping(value = "/document/{id}", method = RequestMethod.GET)
	public @ResponseBody Document getCompany(@PathVariable int id) {

		Document c = null;
		try {
			c = docDao.findOne(id);

		} catch (Exception e) {

			return null;
		}

		return c;
	}

	// GET BY PRODUCT ID
	@RequestMapping(value = "/document/product/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Document> getDocumentsByProduct(
			@PathVariable int id) {

		List<Document> c = null;
		try {

			Product product = productDao.findOne(id);
			c = docDao.findByProduct(product);

		} catch (Exception e) {

			return null;
		}

		return c;
	}

	// Get ALL DOCUMENTS
	@RequestMapping(value = "/document/all", method = RequestMethod.GET)
	public @ResponseBody List<Document> getAllDocuments() {

		List<Document> c = null;
		try {

			c = new ArrayList<Document>();
			docDao.findAll().forEach(c::add);

		} catch (Exception e) {

			return null;
		}

		return c;
	}

	@RequestMapping(value = "/document/type/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Document> getDocumentByType(@PathVariable int id){
        try{
            DocumentStage documentStage = new DocumentStage();
            documentStage.setStageId(id);
            return docDao.findByDocumentStage(documentStage);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ArrayList<>();
	}
}
