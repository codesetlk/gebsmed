package com.gebsmed.controller;

import java.util.ArrayList;
import java.util.List;

import com.gebsmed.dao.CompanyDao;
import com.gebsmed.dao.UserDao;
import com.gebsmed.model.Company;
import com.gebsmed.model.GebsmedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
@RestController
public class CompanyController {

	@Autowired
	CompanyDao companyDao;
	
	@Autowired
	UserDao userDao;

	// CREATE
	/*
	 * { "name":"newCompany" }
	 */
	@RequestMapping(value = "/company/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity createCompany(
			@RequestBody Company company) {

		try {
			companyDao.save(company);

		} catch (Exception e) {
			return new ResponseEntity<>(company, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	// UPDATE
	/*
	 * { "id":10, "name":"editedCompany" }
	 */
	@RequestMapping(value = "/company/update/", method = RequestMethod.PATCH)
	public @ResponseBody ResponseEntity updateCompany(
			@RequestBody Company company) {

		try {
			Company edited = companyDao.findOne(company.getId());
			edited.setName(company.getName());
			companyDao.save(edited);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	// Delete
	/*
	 * Only set the id of company to be deleted
	 */
	@RequestMapping(value = "/company/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteCompany(@PathVariable int id) {

		Company deleted = null;
		try {
			deleted = companyDao.findOne(id);
			companyDao.delete(id);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	// Get One
	@RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
	public @ResponseBody Company getCompany(@PathVariable int id) {

		Company c = null;
		try {
			c = companyDao.findOne(id);
			System.out.println(c.getUsers().toArray().length);

		} catch (Exception e) {

			return null;
		}

		return c;
	}
	
	
	// Get One
	@RequestMapping(value = "/company/user/{id}", method = RequestMethod.GET)
	public @ResponseBody Company getUsersCompany(@PathVariable int id) {

		Company c = null;
		GebsmedUser u=null;
		try {
			u = userDao.findOne(id);
			c = u.getCompany();
			

		} catch (Exception e) {

			return null;
		}

		return c;
	}

	// Get All
	@RequestMapping(value = "/company/all", method = RequestMethod.GET)
	public @ResponseBody List<Company> getAllCompany() {

		List<Company> c = new ArrayList<Company>();
		try {
			companyDao.findAll().forEach(c::add);

		} catch (Exception e) {

			return null;
		}

		return c;
	}

}
