package com.gebsmed.controller;

import com.gebsmed.dao.LogsDao;
import com.gebsmed.model.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

/**
 * Created by polta on 9/21/16.
 */
@RestController
public class LogController {

    @Autowired
    private LogsDao logsDao;

    @RequestMapping(value = "/logs/", method = RequestMethod.GET)
    public @ResponseBody
    Page<Logs> getCompany() {

        Page<Logs> all;
        try {

            all = logsDao.findAll(new PageRequest(1, 10));
        } catch (Exception e) {

            return null;
        }

        return all;
    }
}
