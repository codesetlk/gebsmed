package com.gebsmed.controller;

import java.util.ArrayList;
import java.util.List;

import com.gebsmed.dao.CompanyDao;
import com.gebsmed.dao.ProductDao;
import com.gebsmed.model.Company;
import com.gebsmed.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

	@Autowired
	ProductDao productDao;

	@Autowired
	CompanyDao companyDao;

	// CREATE
	@RequestMapping(value = "/product/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity createProduct(
			@RequestBody Product product) {

		try {
			Company company = companyDao.findOne(product.getCompany().getId());
			product.setCompany(company);
			productDao.save(product);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	// BY COMPANY
	@RequestMapping(value = "/product/company/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Product> getProductByCompany(@PathVariable int id) {

		List<Product> productList = null;
		try {
			Company c = companyDao.findOne(id);
			productList = productDao.findByCompany(c);

		} catch (Exception e) {

			return null;
		}

		return productList;
	}

	// Delete
	@RequestMapping(value = "/product/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteCompany(@PathVariable int id) {

		Product deleted = null;
		try {
			deleted = productDao.findOne(id);
			productDao.delete(id);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	// Update
	@RequestMapping(value = "/product/update/", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateUser(@RequestBody Product product) {

		// System.out.println(company.getName());
		Product edited = null;
		try {
			edited = productDao.findOne(product.getId());
			edited.setName(product.getName());
			edited.setDescription(product.getDescription());

			productDao.save(edited);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(edited, HttpStatus.OK);
	}

	// GET ONE
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public @ResponseBody Product getProduct(@PathVariable int id) {

		Product c = null;
		try {
			c = productDao.findOne(id);

		} catch (Exception e) {

			return null;
		}

		return c;
	}
	
	// Get all users 
	@RequestMapping(value = "/product/all", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Product> getProduct() {

		ArrayList<Product> c = new ArrayList<Product>();
		try {
			productDao.findAll().forEach(c::add);
			

		} catch (Exception e) {

			return null;
		}

		return c;
	}

}
