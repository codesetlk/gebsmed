package com.gebsmed.controller;

/**
 * Created by minindu on 9/19/16.
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @RequestMapping(value="/",method = RequestMethod.GET )
    public String homepage(){
        return "index";
    }

//    @RequestMapping(value="/",method = RequestMethod.POST )
//    public String login(){
//        return "index";
//    }


}
