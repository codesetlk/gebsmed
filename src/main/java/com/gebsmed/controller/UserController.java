package com.gebsmed.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.gebsmed.dao.CompanyDao;
import com.gebsmed.dao.LogsDao;
import com.gebsmed.dao.UserDao;
import com.gebsmed.model.Company;
import com.gebsmed.model.GebsmedUser;
import com.gebsmed.model.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

	@Autowired
	UserDao userDao;

	@Autowired
	CompanyDao companyDao;

	@Autowired
	private LogsDao logsDao;

	// CREATE
	@RequestMapping(value = "/user/create", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity createUser(@RequestBody GebsmedUser user) {

		try {
			Company company = companyDao.findOne(user.getCompany().getId());
			user.setCompany(company);
			userDao.save(user);

			//Create Log for adding new User
			Logs logs = new Logs();
			logs.setTimeStamp(new Date());
			logs.setDescription("new User was sign up");
			logs.setUser(user);
			logsDao.save(logs);
		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	// Update
	@RequestMapping(value = "/user/update/", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateUser(@RequestBody GebsmedUser user) {

		// System.out.println(company.getName());
		GebsmedUser edited = null;
		try {
			edited = userDao.findOne(user.getIdUser());
			edited.setName(user.getName());
			edited.setAuthLevel(user.getAuthLevel());
			edited.setUserName(user.getUserName());
			edited.setPassword(user.getPassword());
			// Company company = companyDao.findOne(user.getCompany().getId());
			// edited.setCompany(company);

			userDao.save(edited);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	// Delete
	@RequestMapping(value = "/user/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteUser(@PathVariable int id) {

		GebsmedUser deleted = null;
		try {
			deleted = userDao.findOne(id);
			userDao.delete(id);

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	// Get
	@RequestMapping(value = "/user/company/{id}", method = RequestMethod.GET)
	public @ResponseBody List<GebsmedUser> getUsersbyCompany(@PathVariable int id) {

		List<GebsmedUser> c = null;
		try {
			Company company = companyDao.findOne(id);
			c = userDao.findByCompany(company);

		} catch (Exception e) {

			return null;
		}

		return c;
	}

	
	
	// Get
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public @ResponseBody GebsmedUser getUser(@PathVariable int id) {

		GebsmedUser c = null;
		try {
			c = userDao.findOne(id);
//			System.out.println(c.getCompany().getName());

		} catch (Exception e) {

			return null;
		}

		return c;
	}
	
	// Get
	@RequestMapping(value = "/user/username/{name}", method = RequestMethod.GET)
	public @ResponseBody List<GebsmedUser> getUserbyUserName(@PathVariable String name) {

		List<GebsmedUser> c = null;
		try {
			c = userDao.findByuserName(name);

		} catch (Exception e) {

			return null;
		}

		return c;
	}
	
	

	// Get all users of role
	@RequestMapping(value = "/user/all/{role}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<GebsmedUser> getAllUsersFromRole(@PathVariable String role) {

		ArrayList<GebsmedUser> c = new ArrayList<GebsmedUser>();
		try {
			Iterator<GebsmedUser> userIterator = userDao.findAll().iterator();
			while (userIterator.hasNext()) {
				GebsmedUser u = userIterator.next();
				System.out.println(u.getAuthLevel());
				if (u.getAuthLevel().equals(role)) {
					c.add(u);
				}

			}

		} catch (Exception e) {

			return null;
		}

		return c;
	}
	
	
	// Get all users 
	@RequestMapping(value = "/user/all/", method = RequestMethod.GET)
	public @ResponseBody ArrayList<GebsmedUser> getAllUsers() {

		ArrayList<GebsmedUser> c = new ArrayList<GebsmedUser>();
		try {
			userDao.findAll().forEach(c::add);
			

		} catch (Exception e) {

			return null;
		}

		return c;
	}
	
	// CREATE
	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity login(@RequestBody GebsmedUser user) {
		GebsmedUser u=null;
		try {
			List<GebsmedUser> c = userDao.findByuserName(user.getUserName());
			 u = c.get(0);
			 if(u.getPassword().equals(user.getPassword())){
				 return new ResponseEntity<>(u, HttpStatus.OK);
			 }else{
				 
				 return new ResponseEntity<>(null,  HttpStatus.BAD_REQUEST);
			 }
			

		} catch (Exception e) {

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		
	}
}
