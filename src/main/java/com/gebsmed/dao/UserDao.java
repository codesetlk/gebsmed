package com.gebsmed.dao;

import java.util.List;

import javax.transaction.Transactional;

import com.gebsmed.model.Company;
import com.gebsmed.model.GebsmedUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserDao extends CrudRepository<GebsmedUser, Integer>{
	public List<GebsmedUser> findByCompany(Company company);
	
	 //@Query("SELECT u FROM GebsmedUser u WHERE u.userName=:username")
	public List<GebsmedUser> findByuserName(String username);

}
