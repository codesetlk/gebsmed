package com.gebsmed.dao;

import java.util.List;

import com.gebsmed.model.Document;
import com.gebsmed.model.DocumentStage;
import com.gebsmed.model.GebsmedUser;
import com.gebsmed.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface DocumentDao extends CrudRepository<Document, Integer> {
	
	
	public List<Document> findByProduct(Product product);

	public List<Document> findByUser(GebsmedUser user);

	public List<Document> findByDocumentStage(DocumentStage documentStage);
	

}
