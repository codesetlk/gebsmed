package com.gebsmed.dao;

import javax.transaction.Transactional;

import com.gebsmed.model.Company;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface CompanyDao extends CrudRepository<Company, Integer>{
	

}
