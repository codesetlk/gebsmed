package com.gebsmed.dao;

import java.util.List;

import javax.transaction.Transactional;

import com.gebsmed.model.Company;
import com.gebsmed.model.Product;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ProductDao extends CrudRepository<Product, Integer> {
	
	public List<Product> findByCompany(Company company);

}
