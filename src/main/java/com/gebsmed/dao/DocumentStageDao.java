package com.gebsmed.dao;

import com.gebsmed.model.DocumentStage;
import org.springframework.data.repository.CrudRepository;

public interface DocumentStageDao extends CrudRepository<DocumentStage, Integer>{

}
