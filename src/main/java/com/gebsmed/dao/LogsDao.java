package com.gebsmed.dao;

import com.gebsmed.model.Logs;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by polta on 9/20/16.
 */
public interface LogsDao extends PagingAndSortingRepository<Logs,  Integer> {

}
