package com.gebsmed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GebsmedApplication {

	public static void main(String[] args) {
		SpringApplication.run(GebsmedApplication.class, args);
	}
}
