package com.gebsmed.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "file_details")
public class FileDetails {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String fileName;
	private String createdDateTime;
	private long size;

	@OneToMany(mappedBy = "fileDetails", cascade = CascadeType.ALL)
	private Set<Logs> logs;

	public void setId(long id) {
		this.id = id;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public FileDetails(long id, String fileName, String createdDateTime, long size){
		this.id = id;
		this.fileName = fileName;
		this.createdDateTime = createdDateTime;
		this.size = size;
	}
	
	public long getId(){
		return id;
	}
	
	public String getFileName(){
		return fileName;
	}
	
	public String getCreatedDate(){
		return createdDateTime;
	}
	
	public long getSize(){
		return size;
	}

	public Set<Logs> getLogs() {
		return logs;
	}

	public void setLogs(Set<Logs> logs) {
		this.logs = logs;
	}
}
