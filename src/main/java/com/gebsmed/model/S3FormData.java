package com.gebsmed.model;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.annotation.JsonProperty;

public class S3FormData {
	
	private String name;
	private String AWSAccessKeyId;
	private String acl;
	private String success_action_redirect;
	private String policy;
	
	@JsonProperty("X-Amz-Signature")
	private String signature;
	
	@JsonProperty("X-Amz-Credential")
	private String amzCredential;
	
	@JsonProperty("X-Amz-Algorithm")
	private String algorithm;
	
	@JsonProperty("X-Amz-Date")
	private String date;
	
	
	
	static byte[] HmacSHA256(String data, byte[] key) throws Exception  {
	     String algorithm="HmacSHA256";
	     Mac mac = Mac.getInstance(algorithm);
	     mac.init(new SecretKeySpec(key, algorithm));
	     return mac.doFinal(data.getBytes("UTF8"));
	}

	static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception  {
	     byte[] kSecret = ("AWS4" + key).getBytes("UTF8");
	     byte[] kDate    = HmacSHA256(dateStamp, kSecret);
	     byte[] kRegion  = HmacSHA256(regionName, kDate);
	     byte[] kService = HmacSHA256(serviceName, kRegion);
	     byte[] kSigning = HmacSHA256("aws4_request", kService);
	     return kSigning;
	}

}
