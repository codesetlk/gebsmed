package com.gebsmed.model;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="document_stage")
public class DocumentStage {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int stageId;

	private String name;

	/*
	 * Sample License - 1
	 * Processing - 2
	 * Primary Registration - 3
	 * Full Registration - 4
	 */
	private Integer type;

	public int getStageId() {
		return stageId;
	}

	public void setStageId(int stageId) {
		this.stageId = stageId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
